Serão adicionados na VW_DISPENSACAO (até terça 09/06):
- VL_PRECO_SUBSIDIADO
- QT_POPULACAO IBGE será quantidade da população segundo a portaria 1555 2013;
- QT_PRESCRITA;
- QT_SOLICITADA;
- QT_ESTORNADA;
Será adicionado: DT_RECEB_CONFIRMACAO e nós desmembramremos:
  - CMP_RECEB_CONFIRMACAO
  - CO_DT_RECEB_CONFIRMACAO


Atributos que já existem na View de Dispensação:
- CO_CODIGO_BARRA = TD_MEDICAMENTO.CO_CODIGO_BARRA
- DS_APRESENTACAO = TM_MEDICAMENTO.DS_APRESENTACAO;
- DS_PRINCIPIO_ATIVO = VW_DISPENSACAO.DS_PRINCIPIO_ATIVO_MEDICAMENTO;
- CO_PRINCIPIO_ATIVO = VW_DISPENSACAO.CO_PRINCIPIO_ATIVO_MEDICAMENTO;
- DS_PATOLOGIA = VW_ENTRADA.DS_PATOLOGIA;
- CO_PATOLOGIA = VW_ENTRADA.CO_PATOLOGIA;
- VL_PRECO_VENDA = TF_DISPENSACAO.VL_UNITARIO; //Preço por comprimido/unidade, caixa com 30 comprimidos é 30*este preço = associado à VL_APRESENTACAO que é o valor de quantidade por unidade farmacotecnica (menor unidade de venda ex.: comprimido, ampola)


VW_ ENTRADA/SAIDA/ESTOQUE = CGMPAF/BNAFAR sem farmpop = MOVIMENTAÇÃO DO MEDICAMENTO entrada em hospitais > departamentos > dispensação
VW_ DISPENSACAO = CGMPAF/BNAFAR/FARMPOP
